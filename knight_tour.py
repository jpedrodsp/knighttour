import operator
import time
import copy
from collections import OrderedDict
import random


def create_matrix(size):
    matrix = []
    for i in range(size):
        line = []
        for j in range(size):
            line.append(0)
        matrix.append(line)
    return matrix

def reset_matrix(matrix):
    length = len(matrix[0])
    for i in range(length):
        for j in range(length):
            matrix[i][j] = 0
    return matrix

def print_matrix(matrix):
    length = len(matrix)
    for i in matrix:
        for j in i:
            print("%02d" % j, end=' ')
        print('')


def matrix_mark(matrix, posx, posy, turn):
    matrix[int(posx)][int(posy)] = turn

def gen_moveset(next_x, next_y):
    moveset = []
    # Normal: No X invert, No Y invert
    x = next_x
    y = next_y
    moveset.append((x, y))
    # Normal: X invert, No Y invert
    x = next_x * -1
    y = next_y
    moveset.append((x, y))
    # Normal: No X invert, Y invert
    x = next_x
    y = next_y * -1
    moveset.append((x, y))
    # Normal: X invert, Y invert
    x = next_x * -1
    y = next_y * -1
    moveset.append((x, y))

    # Alternated: No X invert, No Y invert
    x = next_y
    y = next_x
    moveset.append((x, y))
    # Alternated: X invert, No Y invert
    x = next_y * -1
    y = next_x
    moveset.append((x, y))
    # Alternated: No X invert, Y invert
    x = next_y
    y = next_x * -1
    moveset.append((x, y))
    # Alternated: X invert, Y invert
    x = next_y * -1
    y = next_x * -1
    moveset.append((x, y))

    #print(moveset)
    return moveset

def sum_movement(mov1, mov2):
    return mov1[0] + mov2[0], mov1[1] + mov2[1]

def check_movement_validity(mat, ap, mov, turn=-1):
    size = len(mat)
    nx, ny = sum_movement(ap, mov)
    #print("apx: %d, apy: %d, movx: %d, movy: %d, nx: %d, ny: %d, turn: %d" % (ap[0], ap[1], mov[0], mov[1], nx, ny, turn))
    if nx >= size:
        #print("false")
        return False
    if ny >= size:
        #print("false")
        return False
    if nx < 0:
        #print("false")
        return False
    if ny < 0:
        #print("false")
        return False
    if mat[nx][ny] > 0:
        #print("false")
        return False
    #print("apx: %d, apy: %d, movx: %d, movy: %d, nx: %d, ny: %d, turn: %d ... true!" % (ap[0], ap[1], mov[0], mov[1], nx, ny, turn))
    return True

def bruteforce_test(matrix, starting_pos):
    # Statistics Declaration
    comparisons = 0
    stime = time.time()
    found = False

    # Algorithm
    valid = False
    result = bruteforce_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)
    #result = heuristic_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)
    #result = randomforce_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)

    # End of algorithm
    ftime = time.time()
    dtime = ftime - stime
    return comparisons, dtime, valid

def heuristic_test(matrix, starting_pos):
    # Statistics Declaration
    comparisons = 0
    stime = time.time()
    found = False

    # Algorithm
    valid = False
    #result = bruteforce_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)
    result = heuristic_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)
    #result = randomforce_navigate(matrix, starting_pos, starting_pos, 0, comparisons, valid)

    # End of algorithm
    ftime = time.time()
    dtime = ftime - stime
    return comparisons, dtime, valid

def bruteforce_navigate(matrix, original_position, actual_position, turn=0, comparisons=0, valid=False):
    '''
    A função de força bruta navega pela matriz registrando o número de comparações realizadas
    '''
    max = len(matrix)**2
    # Tenta ir para a posição
    d = check_movement_validity(matrix, actual_position, [0, 0], turn)
    comparisons += 5
    if d:
        turn += 1
        matrix_mark(matrix, actual_position[0], actual_position[1], turn)
        #print_matrix(matrix)
        comparisons += 1
        # Conseguiu chegar no caminho. Verifica condicao de validade.
        comparisons += 1
        if turn >= max:
            #if original_position[0] == actual_position[0] and original_position[1] == actual_position[1]:
            print("Found validity condition.")
            print_matrix(matrix)
            valid = True
            return True
        # Caso ainda nao tenha atingido, tentar novos movimentos
        moveset = gen_moveset(1, -2)
        for move in moveset:
            comparisons += 1
            #print("Calculating new move...")
            newpos = sum_movement(actual_position, move)
            newmatrix = copy.deepcopy(matrix)
            x = bruteforce_navigate(newmatrix, original_position, newpos, turn, comparisons, valid)
            comparisons += 1
            if x:
                valid = True
                return True

def randomforce_navigate(matrix, original_position, actual_position, turn=0, comparisons=0, valid=False):
    '''
    A função de força bruta randômica navega pela matriz registrando o número de comparações realizadas
    '''
    max = len(matrix)**2
    # Tenta ir para a posição
    d = check_movement_validity(matrix, actual_position, [0, 0], turn)
    comparisons += 5
    if d:
        turn += 1
        matrix_mark(matrix, actual_position[0], actual_position[1], turn)
        #print_matrix(matrix)
        comparisons += 1
        # Conseguiu chegar no caminho. Verifica condicao de validade.
        comparisons += 1
        if turn >= max:
            if original_position[0] == actual_position[0] and original_position[1] == actual_position[1]:
                print("Found validity condition.")
                print_matrix(matrix)
                valid = True
                return True
        # Caso ainda nao tenha atingido, tentar novos movimentos
        moveset = gen_moveset(1, -2)
        while (len(moveset) > 0):
            #print(moveset)
            comparisons += 1
            # Choose a random move
            choice = random.randint(0, len(moveset)-1)
            move = moveset.pop(choice)
            # print("Calculating new move...")
            newpos = sum_movement(actual_position, move)
            newmatrix = copy.deepcopy(matrix)
            x = randomforce_navigate(newmatrix, original_position, newpos, turn, comparisons, valid)
            comparisons += 1
            if x:
                valid = True
                return True


def heuristic_navigate(matrix, ip, pos, turn=0, comparisons=0, valid=False):
    '''
    A função de navegação heurística percorre a matriz registrando o número de comparações realizadas
    '''
    max = len(matrix)**2
    # Tenta ir para a posição
    d = check_movement_validity(matrix, pos, [0,0], turn)
    comparisons += 5
    if d:
        turn += 1
        matrix_mark(matrix, pos[0], pos[1], turn)
        #print_matrix(matrix)
        comparisons += 1
        # Conseguiu chegar no caminho. Verifica condicao de validade.
        comparisons += 1
        if turn >= max:
            #comparisons += 1
            #if ip[0] == pos[0] and ip[1] == pos[1]:
            print("Found validity condition.")
            print_matrix(matrix)
            valid = True
            return True
        # Caso ainda nao tenha atingido, tentar novos movimentos
        moveset = gen_moveset(1, -2)
        adj_score_dict = {}
        for move in moveset:
            comparisons += 1
            #print("Calculating new move...")
            newpos = sum_movement(pos, move)
            test = check_movement_validity(matrix, newpos, [0,0], turn)
            comparisons += 1
            if test:
                # Checar as adjacẽncias para o novo ponto
                adjacent_score = 0
                adjacent_moveset = gen_moveset(1, -2)
                for thirdlevel_move in adjacent_moveset:
                    comparisons += 1
                    #print("Calculating third level move...")
                    thirdlevel_pos = sum_movement(newpos, thirdlevel_move)
                    comparisons += 1
                    if thirdlevel_move == newpos:
                        #print("Skipping previous move...")
                        continue
                    _v = check_movement_validity(matrix, thirdlevel_move, [0,0], turn+2)
                    #print("TL-Check for move %d, %d is" % thirdlevel_move, _v)
                    comparisons += 1
                    if _v:
                        #print("Adding score to move (%d, %d)..." % newpos)
                        adjacent_score += 1
                # Armazena o resultado do teste de adjacências
                adj_score_dict[move] = adjacent_score
        # Obtém-se o menor valor da lista de adjacência
        #print ("FLM:", pos)
        #print(adj_score_dict)
        for i in adj_score_dict:
            comparisons += 1
            #print(sum_movement(pos, i), ":", adj_score_dict[i])
        #print("Sorting results into crescent order...")
        _sorted_dict_data = sorted(adj_score_dict.items(), key=lambda kv: kv[1])
        sorted_dict = OrderedDict(_sorted_dict_data)

        while len(_sorted_dict_data) > 0:
            comparisons += 1
            data = _sorted_dict_data.pop(0)
            #print("Processing move", data)
            # DO THE THING
            newmatrix = copy.deepcopy(matrix)
            newpos = sum_movement(pos, data[0])
            x = heuristic_navigate(newmatrix, ip, newpos, turn, comparisons, valid)
            comparisons += 1
            if x:
                valid = True
                return True

        """
        hello world, this is a orphan comment
        """

    pass


if __name__ == "__main__":
    # Start a matrix
    matrix_size = int(input("Size of matrix:"))
    print('Starting a matrix of size', matrix_size, 'x', matrix_size, '...')
    m = create_matrix(matrix_size)

    _spos = None
    while type(_spos) != list:
        print('Insert a position (format: \'x y\'; max %d):' % (matrix_size-1))
        _spos = input()
        _spos = _spos.split(' ')
        #print(_spos)
        if len(_spos) != 2 or int(_spos[0]) >= matrix_size or int(_spos[1]) >= matrix_size:
            _spos = None
            continue
    print('Starting position at: (x:%s, y:%s)' % (_spos[0], _spos[1]))
    #matrix_mark(m, _spos[0], _spos[1], 1)
    _spos[0] = int(_spos[0])
    _spos[1] = int(_spos[1])
    print_matrix(m)

    file = open("bftest.txt", "a")
    data = str(bruteforce_test(m, _spos))
    file.write(data + "\n")
    file.close()
    file = open("hetest.txt", "a")
    data = str(heuristic_test(m, _spos))
    file.write(data + "\n")
    file.close()